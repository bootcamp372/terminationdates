﻿using Microsoft.VisualBasic;
using System.Xml.Linq;

namespace TerminationDates
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Policy Renewal Date: ");
            string input = Console.ReadLine();
            DateTime inputDate; 
/*                = Convert.ToDateTime(Console.ReadLine());*/
            bool didWork = DateTime.TryParse(input, out inputDate);
            if (didWork)
            {
                Console.WriteLine("Date read is: {0}", inputDate.ToString());
            }
            else
            {
                Console.WriteLine("Input is not a valid date");
                return;
            }


/*            DateTime today = DateTime.Now;*/
            DateTime gracePeriod = inputDate.AddDays(10);
            DateTime cancellationDate = inputDate.AddMonths(1); ;
            Console.WriteLine($"10 day Grace period ends on: {gracePeriod}");
            Console.WriteLine($"Cancellation Date is: {cancellationDate}");
        }
    }
}