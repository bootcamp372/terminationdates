﻿using System.Threading.Tasks;

namespace Greeter
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*            TimeSpan currentTime = DateTime.Now.TimeOfDay;*/
            System.DateTime dt = DateTime.Now;
            int hours = dt.Hour;
            Console.WriteLine("Datetime: " + dt);
            Console.WriteLine("Hours: " + hours);

            if (hours >= 5 && hours < 10) {
                Console.WriteLine("Good Morning!");
            } else if (hours >= 10 && hours < 18) {
                Console.WriteLine("Good Day!");
            } else if (hours >= 19 && hours <= 24)
            {
                Console.WriteLine("Good Evening!");
            }
            else if (hours < 5) {
                Console.WriteLine("Welcome to the late shift!");
            } else {
                Console.WriteLine("Invalid Datetime");
            }
        }
    }
}